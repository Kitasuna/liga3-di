package ru.digitalleague;

import ru.digitalleague.domain.User;
import ru.digitalleague.injection.ComponentScanner;
import ru.digitalleague.injection.Container;
import ru.digitalleague.services.UserService;

public class Main {

    public static void main(String[] args) {
        Container container = new Container(new ComponentScanner());
        container.instantiate(UserService.class).findAll().stream().map(User::getName).forEach(System.out::println);
    }
}
