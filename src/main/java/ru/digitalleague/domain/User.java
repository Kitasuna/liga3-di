package ru.digitalleague.domain;

import java.util.List;
import java.util.UUID;

public class User {
    private UUID id;
    private String name;
    private School school;

    public User(UUID id, String name, School school) {
        this.id = id;
        this.name = name;
        this.school = school;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public School getSchool() {
        return school;
    }
}
