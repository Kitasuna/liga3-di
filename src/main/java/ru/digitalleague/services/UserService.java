package ru.digitalleague.services;

import ru.digitalleague.domain.School;
import ru.digitalleague.domain.User;

import java.util.List;
import java.util.UUID;

public class UserService {
    private final SchoolService schoolService;

    public UserService(SchoolService schoolService) {
        this.schoolService = schoolService;
    }

    public List<User> findAll() {
        List<School> schools = schoolService.findAll();

        return List.of(
                new User(UUID.randomUUID(), "Пользователь №1", getRandomItem(schools)),
                new User(UUID.randomUUID(), "Пользователь №2", getRandomItem(schools)),
                new User(UUID.randomUUID(), "Пользователь №2", getRandomItem(schools)),
                new User(UUID.randomUUID(), "Пользователь №4", getRandomItem(schools)),
                new User(UUID.randomUUID(), "Пользователь №5", getRandomItem(schools)),
                new User(UUID.randomUUID(), "Пользователь №6", getRandomItem(schools))
        );
    }

    private <T> T getRandomItem(List<T> items) {
        if (items.isEmpty()) {
            return null;
        }

        return items.get((int) Math.floor(Math.random() * items.size()));
    }
}
