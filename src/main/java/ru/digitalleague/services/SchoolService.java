package ru.digitalleague.services;

import ru.digitalleague.domain.School;

import java.util.List;
import java.util.UUID;

public class SchoolService {
//    private final UserService userService;

//    public SchoolService(UserService userService) {
////        this.userService = userService;
//    }

//    public SchoolService() {
////        this.userService = userService;
//    }

    public List<School> findAll() {
        return List.of(
                new School(UUID.randomUUID(), "Школа №1"),
                new School(UUID.randomUUID(), "Школа №2"),
                new School(UUID.randomUUID(), "Школа №3"),
                new School(UUID.randomUUID(), "Школа №4"),
                new School(UUID.randomUUID(), "Школа №5")
        );
    }
}
