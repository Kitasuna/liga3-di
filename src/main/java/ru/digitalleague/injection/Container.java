package ru.digitalleague.injection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

public class Container {
    private final HashMap<Class<?>, ClassInfo<?>> dependencyTree = new HashMap<>();
    private final List<Class<?>> antiCircle = new ArrayList<>();

    public Container(ComponentScanner componentScanner) {
        componentScanner.getComponents().forEach(clazz -> {
            Constructor<?>[] constructors = clazz.getConstructors();
            if (constructors.length > 1) {
                throw new RuntimeException("Too many constructors");
            }

            Constructor<?> constructor = constructors[0];

            dependencyTree.put(clazz, new ClassInfo<>(Arrays.asList(constructor.getParameterTypes()), constructor));
        });
    }

    public <T> T instantiate(Class<T> clazz) {
    }

    private static class ClassInfo<T> {
        private final List<Class<?>> arguments;

        private final Constructor<T> constructor;

        public ClassInfo(List<Class<?>> arguments, Constructor<T> constructor) {
            this.arguments = arguments;
            this.constructor = constructor;
        }

        public List<Class<?>> getArguments() {
            return arguments;
        }

        public Constructor<T> getConstructor() {
            return constructor;
        }
    }
}
