package ru.digitalleague.injection;

import ru.digitalleague.services.SchoolService;
import ru.digitalleague.services.UserService;

import java.util.Set;

public class ComponentScanner {
    public Set<Class<?>> getComponents() {
        /// костыль чтобы не упарываться в полное чтение пакета. у идеальном мире оно не захардкожено :)
        return Set.of(UserService.class, SchoolService.class);
    }
}
